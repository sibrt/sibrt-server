#!/usr/bin/env python3
import eventlet
eventlet.monkey_patch()
import traceback
from flask import Flask
from flask_socketio import SocketIO
from flask import request
import json
from sibrt.server import Server
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)
socketio = SocketIO(app, engineio_logger=False,logger=False,async_mode='eventlet')
socketio.async_handlers=True
S=Server(socketio)
try:
    S.LoadRezept()
except:
    traceback.print_exc()
    print("unable to load stored Rezept")


Success="Success"

@socketio.on('update')
def handle_message(message):
    if(isinstance(message, (dict))):
        for a in message.keys():
            S.UpdateDatastream(a,{"value":message[a]})


@app.route(S.apiurl+'/datastreams',methods=['GET'])
def get_datastreams():
    return json.dumps(S.ListDatastreams(type=request.args.get('type')))

@app.route(S.apiurl+'/datastreams/<id>',methods=['GET'])
def get_datastream(id):
    return json.dumps(S.GetDatastream(id))

@app.route(S.apiurl+'/datastreams/<id>',methods=['POST'])
def set_datastreams(id):
    S.UpdateDatastream(id,json.loads(request.json))
    return Success

@app.route(S.apiurl+'/rezept',methods=['GET'])
def get_rezept():
    if(request.args.get('do')!=None):
        S.Action(request.args.get('do'))
    if(request.args.get('goto')!=None):
        S.Goto(request.args.get('goto'))
    return json.dumps(S.GetRezept())
@app.route(S.apiurl+'/rezept',methods=['POST'])
def set_rezept():
    S.SetRezept(request.json)
    return Success
@app.route(S.apiurl+'/rezept/steps',methods=['GET'])
def get_steps():
    return json.dumps(S.GetSteps())
@app.route(S.apiurl+'/rezept/steps/current',methods=['GET'])
def get_currentstep():
    return json.dumps(S.GetCurrentStep())


@app.route('/')
def hello():
    return app.send_static_file('index.html')


if __name__ == "__main__":
    import sys
    port=5000
    if("-p" in sys.argv):
        port=sys.argv[sys.argv.index("-p")+1]
    S.start()
    socketio.run(app, host= '0.0.0.0', port=port)
    S.stop()

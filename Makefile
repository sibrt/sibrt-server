.PHONY: vuejsfrontend static clean dist
python=python3
VERSION="0.1"
BUILD=$(shell date +%Y%m%d%H%M)
NAME=sibrt-server
PKG=${NAME}-${VERSION}-${BUILD}
DPKG_ARCH:=all
DPKG=dpkg-deb


all: static

vuejsfrontend:
	cd $@ && npm run build

static/google.css:
	mkdir -p static
	wget -O $@ "fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons"
	grep material-icons $@ > /dev/null
	echo "@font-face{ font-family:'Material Icons'; src: url('/static/materialicon.ttf'); }" >> $@

static/materialicon.ttf: 
	mkdir -p static
	wget -O $@ "http://fonts.gstatic.com/s/materialicons/v55/flUhRq6tzZclQEJ-Vdg-IuiaDsNZ.ttf"

static: vuejsfrontend static/google.css static/materialicon.ttf
	mkdir -p $@
	cp -r $</dist/* $@/
	cp static-content/alarm.mp3 $@/
	cp static-content/ring.mp3 $@/

dist: release/${PKG}.deb

build/${PKG}: static
	mkdir -p $@/usr/share/${NAME}
	mkdir -p $@/usr/lib/systemd/system/
	mkdir -p $@/etc/default/
	mkdir -p $@/usr/share/${NAME}/sibrt
	mkdir -p $@/var/db/sibrt
	cp -r static $@/usr/share/${NAME}/static
	cp -r sibrt/*.py $@/usr/share/${NAME}/sibrt/
	cp server.py $@/usr/share/${NAME}/server.py
	chmod 755 $@/usr/share/${NAME}/server.py
	cp dist/${NAME}.service $@/usr/lib/systemd/system/
	mkdir -p $@/DEBIAN
	echo "systemctl daemon-reload" > $@/DEBIAN/postinst
	chmod 755 $@/DEBIAN/postinst
	cp dist/dpkg-control $@/DEBIAN/control
	echo Version: ${VERSION}-${BUILD} >> $@/DEBIAN/control
	echo Architecture: ${DPKG_ARCH} >> $@/DEBIAN/control

release/${PKG}.deb: build/${PKG}
	mkdir -p release
	${DPKG} -b $< $@

clean:
	rm -rf static
	rm -rf build

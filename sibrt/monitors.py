class Monitor:
    Running=2
    Waiting=1
    Done=0
    def __init__(self):
        self.status=Monitor.Running
    def Status(self):
        return self.status
    def Run(self,step,ds):
        pass


class MonTemperature(Monitor):
    def __init__(self,id):
        Monitor.__init__(self)
        self.tempstream=id
    def Run(self,Server):
        step=Server.GetCurrentStep()
        res={"soll":None,"min":None,"max":None}
        if("temp" in step.keys()):
            res={"soll":step["temp"],"min":step["temp"],"max":step["temp"]}
            for a in ["min","max"]:
                if(a in step.keys()):
                    res[a]=step[a]
        Server.UpdateDatastream(self.tempstream,res)
        stream=Server.GetDatastream(self.tempstream)
        if("trigger" not in stream.keys() or stream['trigger']==True):
            self.status=Monitor.Done
        else:
            self.status=Monitor.Waiting

class MonTimer(Monitor):
    def __init__(self,id,trigger):
        Monitor.__init__(self)
        self.id=id
        self.trigger=trigger
    def Run(self,Server):
        step=Server.GetCurrentStep()
        if("time" not in step.keys() or step["time"]==0 or step["time"]==None):
            Server.Action("StopTimer")
        else:
            Server.LoadTimer(step["time"])

        temp=Server.GetDatastream(self.trigger)
        if("trigger" not in temp.keys() or temp['trigger']==True):
            Server.Action("StartTimer")

        status=Server.GetTimer()
        if(status['status']=="ended"):
            self.status=Monitor.Done
        if(status['status']=="off"):
            self.status=Monitor.Done
        if(status['status']=="running"):
            self.status=Monitor.Running
        if(status['status']=="wait"):
            self.status=Monitor.Waiting
        Server.UpdateDatastream("timer",status)

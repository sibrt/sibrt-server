from os.path import join
import json

class JsonFile:
    def __init__(self):
        self.data={}
        self.dir=""
    def Load(self,filename):
        file = open(join(self.dir,filename))
        filecontent = file.read()
        file.close()
        self.data=json.loads(str(filecontent))
    def Store(self,filename):
        with open(join(self.dir,filename), 'w') as outfile:
            json.dump(self.data, outfile, indent=2)

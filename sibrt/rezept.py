from . import Log
class Rezept:
    def __init__(self):
        self.data={}
        self.steps=[]
        self.currentStep=-1
        self.running=False
        self.log=Log("Rezept")
        self.log.level=Log.Info
        self.Update({"einmaischen":1,"maischen":[],"abmaischen": 2,"kochen": 80,"hopfen":[]})
    def Start(self):
        self.log.I("Start")
        self.currentStep=0
        self.running=True
    def Stop(self):
        self.log.I("Stopp")
        self.running=False
        self.currentStep=-1
    def Status(self):
        return self.running
    def Update(self,data):
        self.log.I("aktualisiert")
        if not type(data) is dict:
            return
        for a in ["einmaischen","abmaischen","kochen"]:
            if(a in data.keys()):
                try:
                    self.data[a]=int(data[a])
                except:
                    self.data[a]=0
        self.data["maischen"]=[]
        if("maischen" in data.keys()):
            for a in data["maischen"]:
                try:
                    self.data["maischen"].append({'tp':int(a['tp']),'tt':int(a['tt'])})
                except:
                    pass
        self.data["hopfen"]=[]
        if("hopfen" in data.keys()):
            for a in data["hopfen"]:
                try:
                    res={}
                    if('text' in a.keys()):
                        res['text']=str(a['text'])
                    if('tt' in a.keys()):
                        res['tt']=int(a['tt'])
                    self.data["hopfen"].append(res)
                except:
                    pass
        self.steps=self._getSteps()
        self.Stop()
    def GetDict(self):
        for a in ["hopfen","maischen"]:
            if(a not in self.data.keys()):
                self.data[a]=[]
        return self.data
    def GetSteps(self):
        return self.steps
    def GetCurrentStep(self):
        if(self.running!=True):
            return None
        if(self.currentStep<0):
            return None
        return self.steps[self.currentStep]
    def _getSteps(self):
        res=[]
        i=0
        res.append(Step(i,"Einmaischen",None,self.data["einmaischen"]))
        i+=1
        for a in self.data['maischen']:
            res.append(Step(i,"%d. Rast"%i,a['tt'],a['tp']))
            i+=1
        res.append(Step(i,"Abmaischen",None,self.data["abmaischen"]))
        i+=1
        kochen=Step(i,"Kochen",self.data["kochen"],100,2,10)
        for a in self.data['hopfen']:
            text=""
            if('text' in a.keys()):
                text=a['text']
            if('tp' in a.keys()):
                kochen.AddExtraTimer(text,a['tp'])
        res.append(kochen)
        return res
    def Goto(self,n):
        if(self.running!=True):
            return
        if(n>=len(self.steps)):
            return
        self.log.I("Gehe zu Step %d",n)
        self.currentStep=n
    def Next(self):
        if(self.running!=True):
            return
        self.log.I("Gehe zum nächsten Step")
        self.currentStep+=1
        if(self.currentStep>=len(self.steps)):
            self.Stop()

class Step:
    def __init__(self,id=-1,name="",time=None, temp=None,mintemp=0.5,maxtemp=0.5):
        self.data={'id':id,'name':name,'time':time,'temp':temp, 'min':temp,'max':temp}
        if(mintemp!=None and temp!=None):
            self.data['min']=temp-mintemp
        if(maxtemp!=None and temp!=None):
            self.data['max']=temp+maxtemp
    def AddExtraTimer(self,text,time):
        if("timers" not in self.data.keys()):
            self.data[timers]=[]
        self.data['timers'].append({'time':time,'text':text})
    def GetDict(self):
        return self.data

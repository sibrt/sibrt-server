from time import time
from . import Log

class Timer:
    def __init__(self):
        self.start=0
        self.time=0
        self.log=Log("Timer")
        self.log.level=Log.Info
    def Load(self,time):
        if(self.time!=time):
            self.log.I("Setzte auf %d",time)
            self.Stop()
        self.time=time
    def Start(self):
        if(self.start==0 and self.time>0):
            self.log.I("Start")
            self.start=time()
    def Left(self):
        if(self.start==0):
            return self.time
        return self.time-(time()-self.start)
    def Stop(self):
        if(self.start>0):
            self.log.I("Stopp")
        self.start=0
        self.time=0
    def Status(self):
        if(self.time>0 and self.start>0):
            if(self.Left()<=0):
                return "ended"
            return "running"
        if(self.time>0):
            return "wait"
        return "off"

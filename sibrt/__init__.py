from time import time, sleep


class SleepAbsolut:
    def __init__(self):
        self.start=time()
    def Sleep(self,t):
        a=(self.start+t)-time()
        sleep(a)
        self.start=time()

class Log:
    NoLog=0
    Error=1
    Warn=2
    Info=3
    Debug=4
    def __init__(self,id=None):
        self.id=id
        self.level=self.Warn
    def Log(self,level,Message,Values=[]):
        if(level>=self.level):
            pre=""
            if(self.id!=None):
                pre="[%s]\t"%self.id
            print(pre+Message%Values)
    def D(self,Message,Values=[]):
        self.Log(self.Debug,Message,Values)
    def I(self,Message,Values=[]):
        self.Log(self.Info,Message,Values)
    def W(self,Message,Values=[]):
        self.Log(self.Warn,Message,Values)
    def E(self,Message,Values=[]):
        self.Log(self.Error,Message,Values)

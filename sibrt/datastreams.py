import time
from . import Log

class DataStreams:
    def __init__(self):
        self.data={}
        self.default_timeout=10
        self.log=Log("DataStreams")
        self.log.level=Log.Info
        self.emit_target=5
    def Get(self,id):
        a=None
        if(id in self.data.keys()):
            a=self.data[id]
            if('timestamp' in a.keys()):
                a['age']=time.time()-a['timestamp']
        return a
    def Update(self,id,values):
        res={}
        if(id not in self.data.keys()):
            self.data[id]={'id':id,'timeout':self.default_timeout,'type':"external",'emittarget':self.emit_target}

        self.data[id]['value_changed']=False

        for a in values.keys():
            if(values[a]==None):
                if(a in self.data[id].keys()):
                    del self.data[id][a]
                continue
            if(a in ['soll']):
                if(a not in self.data[id].keys() or self.data[id][a]!=values[a]):
                    self.log.I("Reset Trigger für <%s>",id)
                    self.data[id]['trigger']=False
            if(a in ['value','timeout','min','max','soll']):
                if(isinstance(values[a], (int, float))):
                    if(a=='value'):
                        self.data[id]['timestamp']=time.time()
                        if("value" not in self.data[id].keys()):
                            self.data[id]['value_changed']=True
                        else:
                            self.data[id]['value_changed']=(self.data[id][a]!=values[a])
                    self.data[id][a]=values[a]
            if(a in ['status','reason','trigger','type','lastemit','emittarget']):
                self.data[id][a]=values[a]
        if("status" not in self.data[id].keys()):
            self.data[id]['status']='ok'
            self.data[id]['reason']=''
        return self.data[id]
    def List(self,type=None):
        if(type==None):
            return  list(self.data.keys())
        res=[]
        for a in self.data.keys():
            if("type" in self.data[a].keys() and self.data[a]['type']==type):
                res.append(a)
        return res
    def Check(self,id):
        res={"status":'ok','reason':''}
        a=self.Get(id)
        if(a==None):
            self.log.I("Kein Daten für <%s>",id)
            return {"status":'error','reason':'No Data'}
        if('type' in a.keys() and a['type']=='timer'):
            return res
        if('value' in a.keys()):
            if('min' in a.keys()):
                if(a['min']>a['value']):
                    res={"status":'error','reason':'value'}
            if('max' in a.keys()):
                if(a['max']<a['value']):
                    res={"status":'error','reason':'value'}
            if('trigger' in a.keys() and a['trigger']==False):
                if(res['status']=='ok'):
                    self.log.I("Trigger für <%s> aktiviert",id)
                    res['trigger']=True
                else:
                    res={"status":'wait','reason':'trigger'}
        if('age' in a.keys() and 'timeout' in a.keys()):
            if(a['timeout']<a['age']):
                res={"status":'error','reason':'timeout','value':None}
        return self.Update(id,res)
    def FormatStream(self,d):
        res={'name':d['id']}
        for i in ['value','status',"soll","min","max"]:
            if(i in d.keys()):
                res[i]=d[i]
        return res

import time
import traceback
from .rezept import Rezept
from .timer import Timer
from .datastreams import DataStreams
from .store import JsonFile
import threading
from . import SleepAbsolut
from .monitors import *

"""
* Achtung: Alle Methoden dieser Klasse müssen Thread-Save sein
"""


class Server(threading.Thread):
    def __init__(self,socketio):
        threading.Thread.__init__(self)
        self.socketio=socketio
        self.apiurl="/api/v2"
        self.datastreams=DataStreams()
        self.Ldatastreams=threading.Lock()
        self.rezept=Rezept()
        self.Lrezept=threading.Lock()
        self.timer=Timer()
        self.Ltimer=threading.Lock()
        self.sleep=SleepAbsolut()
        self.running=True
        self.storedRezept=JsonFile()
        self.storedRezept.dir="/var/db/sibrt"
        self.monitors=[]
        self.monitors.append(MonTemperature("temp1"))
        self.monitors.append(MonTimer("timer","temp1"))
    def run(self):
        self.running=True
        while(self.running):
            for m in self.monitors:
                m.Run(self)
            self.CheckDataStreams()
            next=True
            for m in self.monitors:
                if(m.Status()!=Monitor.Done):
                    next=False
            if(next==True):
                self.Next()
            self.sleep.Sleep(1)
    def stop(self):
        self.running=False
        self.join()
    def CheckDataStreams(self):
        res=[]
        self.Ldatastreams.acquire()
        try:
            for a in self.datastreams.List():
                d=self.datastreams.Check(a)
        except Exception as e:
            self.Ldatastreams.release()
            raise e
        self.Ldatastreams.release()
    def EmitUpdate(self,id,data):
        if(data==None):
            return
        d=self.datastreams.FormatStream(data)
        self.UpdateDatastream(id,{"lastemit":time.time()},emit=False)
        self.socketio.emit(id,d)
    def LoadTimer(self,time):
        self.Ltimer.acquire()
        try:
            self.timer.Load(time*60)
        except Exception as e:
            self.Ltimer.release()
            raise e
        self.Ltimer.release()
    def GetTimer(self):
        res={'type':'timer'}
        self.Ltimer.acquire()
        try:
            res['total']=self.timer.time
            res['value']=self.timer.Left()
            res['status']=self.timer.Status()
        except Exception as e:
            self.Ltimer.release()
            raise e
        self.Ltimer.release()
        return res
    def LoadRezept(self):
        self.storedRezept.Load("Rezept.json")
        self.SetRezept(self.storedRezept.data)
    def GetDatastream(self,id):
        a={}
        self.Ldatastreams.acquire()
        try:
            a=self.datastreams.Get(id)
            if(a!=None):
                a=a.copy()
        except Exception as e:
            self.Ldatastreams.release()
            raise e
        self.Ldatastreams.release()
        return a
    def UpdateDatastream(self,id,values,emit=True):
        res=None
        self.Ldatastreams.acquire()
        try:
            res=self.datastreams.Update(id,values)
        except Exception as e:
            self.Ldatastreams.release()
            raise e
        self.Ldatastreams.release()
        if(emit==False):
            return
        if('value_changed' in res.keys() and res['value_changed']==True):
            self.EmitUpdate(id,res)
            return
        if('status' in res.keys() and res['status'] not in ["ok","off"]):
            self.EmitUpdate(id,res)
            return
        if('lastemit' not in res.keys() or res['lastemit']<(time.time()-res['emittarget'])):
            self.EmitUpdate(id,res)
            return
    def ListDatastreams(self,type):
        self.Ldatastreams.acquire()
        try:
            res= self.datastreams.List(type)
        except Exception as e:
            self.Ldatastreams.release()
            raise e
        self.Ldatastreams.release()
        return res
    def GetRezept(self):
        self.Lrezept.acquire()
        try:
            res=self.rezept.GetDict()
        except Exception as e:
            self.Lrezept.release()
            raise e
        self.Lrezept.release()
        return res
    def SetRezept(self,rezept):
        self.Lrezept.acquire()
        try:
            self.rezept.Update(rezept)
            self.storedRezept.data=self.rezept.data
        except Exception as e:
            self.Lrezept.release()
            raise e
        try:
            self.storedRezept.Store("Rezept.json")
        except Exception as e:
            print("unable to store rezept permanently:")
            traceback.print_exc()
        self.Lrezept.release()
    def GetSteps(self):
        res=[]
        self.Lrezept.acquire()
        try:
            a=self.rezept.GetSteps()
            for i in a:
                res.append(i.GetDict())
        except Exception as e:
            self.Lrezept.release()
            raise e
        self.Lrezept.release()
        return res
    def GetCurrentStep(self):
        res={}
        self.Lrezept.acquire()
        try:
            a=self.rezept.GetCurrentStep()
            if(a!=None):
                res=a.GetDict().copy()
        except Exception as e:
            self.Lrezept.release()
            raise e
        self.Lrezept.release()
        return res
    def Action(self,type):
        if(type=="start"):
            self.Lrezept.acquire()
            try:
                self.rezept.Start()
            except Exception as e:
                self.Lrezept.release()
                raise e
            self.Lrezept.release()
        if(type=="stop"):
            self.Lrezept.acquire()
            try:
                self.rezept.Stop()
            except Exception as e:
                self.Lrezept.release()
                raise e
            self.Lrezept.release()
        if(type=="StartTimer"):
            self.Ltimer.acquire()
            try:
                self.timer.Start()
            except Exception as e:
                self.Ltimer.release()
                raise e
            self.Ltimer.release()
        if(type=="StopTimer"):
            self.Ltimer.acquire()
            try:
                self.timer.Stop()
            except Exception as e:
                self.Ltimer.release()
                raise e
            self.Ltimer.release()
    def Goto(self,n):
        self.Lrezept.acquire()
        try:
            self.rezept.Goto(int(n))
        except Exception as e:
            self.Lrezept.release()
            raise e
        self.Lrezept.release()
    def Next(self):
        self.Lrezept.acquire()
        try:
            self.rezept.Next()
        except Exception as e:
            self.Lrezept.release()
            raise e
        self.Lrezept.release()

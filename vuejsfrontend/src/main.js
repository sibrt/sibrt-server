import Vue from 'vue';
import App from './App.vue';
import VueSocketio from 'vue-socket.io';
import { MdIcon } from 'vue-material/dist/components'
import { MdButton } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

Vue.use(new VueSocketio({
debug: false,
connection: '/'
}));

Vue.use(MdIcon);
Vue.use(MdButton);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  sockets:{
    connect: function(){
      console.log('socket connected')
    }
  }
}).$mount('#app')
